package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import main.BezierGUI.State;

import org.apache.commons.math3.util.ArithmeticUtils;
import org.junit.Test;

public class DrawPanel extends Panel {
	private static final int dragTolerance = 20;
	private static final int controlPointSize = 10;
	
	private List<Point2D> controlPoints = new ArrayList<>();
	private List<Point2D> curvePoints = new ArrayList<>();
	private Rectangle selectionFrame = new Rectangle(20,20);
	private boolean drawSelectionFrame = false;
	private Point2D draggedPoint = null;
	
	public DrawPanel() {
		super();
	}

	@Override
	public void paint(Graphics g) {
		
		clearWhiteBackground(g);
		g.setColor(Color.BLACK);
		g.drawString(controlPoints.size() + " / 20", 5, 20);
		
		for (Point2D point : controlPoints) {
			
			g.fillArc((int) point.getX()-controlPointSize/2, (int) point.getY()-controlPointSize/2, controlPointSize, controlPointSize, 0, 360);
			
		}
		
		for (int i = 0; i < curvePoints.size() - 1; i++) {

			g.drawLine((int)curvePoints.get(i).getX(), (int)curvePoints.get(i).getY(), (int)curvePoints.get(i + 1).getX(), (int)curvePoints.get(i + 1).getY());
		}

		g.setColor(Color.RED);
		if (drawSelectionFrame) {
			g.drawRect(selectionFrame.x-selectionFrame.width/2, selectionFrame.y-selectionFrame.height/2, selectionFrame.width, selectionFrame.height);
		}
	}

	private void clearWhiteBackground(Graphics g) {
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
	}

	public void addPoint(Point point) {
		if(controlPoints.size() >= 20) {
			System.out.println("No more points to prevent arithmetic exceptions...");
			return;
		}
		
		controlPoints.add(point);
		calcCurvePoints();
		this.repaint();
	}
	
	public Point2D getSelectedPoint(Point mousePosition) {
		for (Point2D point : controlPoints) {
			if (pointInSelection(point, mousePosition)) {
				return point;
			}
		}
		return null;
	}
	
	public boolean pointInSelection(Point2D point, Point mousePosition) {
		if(epsilonEqual(point.getX(), mousePosition.getX(), dragTolerance) && epsilonEqual(point.getY(), mousePosition.getY(), dragTolerance)) {
			return true;
		}
		return false;
	}

	public static boolean epsilonEqual(double x, double reference, int tolerance) {
		if (x >= reference-tolerance && x<= reference+tolerance) {
			return true;
		}
		return false;
	}
	
	public void setSelectionFramePosition(Point2D selection) {
		setSelectionFramePosition((int)selection.getX(), (int)selection.getY());
	}

	public void setSelectionFramePosition(int x, int y) {
		selectionFrame.x = x;
		selectionFrame.y = y;
	}

	// Calculates the points of the corresponding curve with Bernstein poylnoms.
	// The first for-loop iterates over the interval k [0-1] in which the
	// curve is defined.
	public void calcCurvePoints() {
		curvePoints = new ArrayList<>();

		int L = controlPoints.size() - 1;
		float bernstein1 = 0;
		float bernstein2 = 0;
		
		for (float width = 0; width <= 1000; width += 0.1) {
			float t = width/1000.0f;
			Point newPoint = new Point();
			for(int k  = 0; k <= L; k++) {
				bernstein1 = ArithmeticUtils.factorial(L) / (ArithmeticUtils.factorial(k) * ArithmeticUtils.factorial(L-k));
				bernstein2 = (float) (Math.pow((1-t), L-k) * Math.pow(t,k));
				newPoint.x += bernstein1*bernstein2*controlPoints.get(k).getX();
				newPoint.y += bernstein1*bernstein2*controlPoints.get(k).getY();
			}
			curvePoints.add(newPoint);
		}
		
	}

	public Point2D getDraggedPoint() {
		return draggedPoint;
	}

	public void setDraggedPoint(Point2D draggedPoint) {
		this.draggedPoint = draggedPoint;
	}

	public void clearControlPoints() {
		controlPoints = new ArrayList<>();
		curvePoints = new ArrayList<>();
		repaint();
		
	}

	public boolean isDrawSelectionFrame() {
		return drawSelectionFrame;
	}

	public void setDrawSelectionFrame(boolean drawSelectionFrame) {
		this.drawSelectionFrame = drawSelectionFrame;
	}
	
}
